<?php

    include_once("Conexion_bd.php");

    class AlumnoDAO {

        private $conexion;

        public function __construct() {
            $this->conexion = new ConexionBD();
        }

        // Metodo ABCC
        // ============================================== ALTAS ============================================== 
        
        public function agregar($NumControl, $name, $fAp, $sAp, $age, $sem, $car){

            $stmt = mysqli_prepare($this->conexion->getConection(), "INSERT INTO alumnos VALUES (?,?,?,?,?,?,?)");
            mysqli_stmt_bind_param($stmt, 'ssssiis', $NumControl, $name, $fAp, $sAp, $age, $sem, $car);
            mysqli_stmt_execute($stmt);
            printf("%d Row inserted.\n", mysqli_stmt_affected_rows($stmt));

            if ( mysqli_stmt_affected_rows($stmt) > 0 ) {
                mysqli_stmt_close($stmt);
                header("location:../Vistas/FormularioAltas.php");
            } else {
                mysqli_stmt_close($stmt);
                header("location:../Vistas/FormularioAltas.php");
            }

        }

        // ============================================== BAJAS ============================================== 

        public function eliminar($NumControl){

            $sql = "DELETE FROM alumnos WHERE Num_Control ='$NumControl'";

            $resultado = mysqli_query($this->conexion->getConection(), $sql);

            if ( $resultado) {
                // echo "<br> Exito";
                echo "<script>alert('Eliminado con exito.')</script>";
                header("location:../vistas/pantalla_Eliminar.php");
            } else {
                echo "<br> Fracaso";
                mysqli_error($this->conexion->getConection());
            }


        }

        // ============================================= CAMBIOS ============================================= 
        public function modificar($NumControl, $name, $fAp, $sAp, $age, $sem, $car){

            $stmt = mysqli_prepare($this->conexion->getConection(), "UPDATE alumnos SET Nombre_Alumno = ?, Primer_Ap_Alumno = ?, Segundo_Ap_Alumno = ?, edad = ?, Semestre = ?, Carrera = ? WHERE Num_Control=?");
            mysqli_stmt_bind_param($stmt, 'sssiiss', $name, $fAp, $sAp, $age, $sem, $car, $NumControl);

            if ( mysqli_stmt_execute($stmt) ) {
                mysqli_stmt_close($stmt);
                header("location:../Vistas/pantalla_Modificar.php");
                // return true;
            } else {
                mysqli_stmt_close($stmt);
                header("location:../Vistas/FormularioAltas.php");
                //return false;
            }

        }

        
        // ============================================ CONSULTAS ============================================
        public function mostrarTodos(){

            $sql = "SELECT * FROM ALUMNOS";

            $resultado = mysqli_query($this->conexion->getConection(), $sql);

            if ( $resultado ) {
                return$resultado;
                // header("location:../vistas/bajas_cambios.php");
            } else {
                echo "<br> FRACASO EN CONSULTA";
                mysqli_error($this->conexion->getConection());
            }


        }

        public function consultaEspesifica($Busqueda) {

            $stmt = mysqli_prepare($this->conexion->getConection(), "SELECT * FROM alumnos WHERE Num_Control like ? OR Nombre_Alumno like ? OR Primer_Ap_Alumno like ? OR Segundo_Ap_Alumno like ? OR edad = ? OR Semestre = ? OR Carrera like ?");
            $BusquedaINT = $Busqueda;
            $BusquedaTXT = "%".$Busqueda."%";
            mysqli_stmt_bind_param($stmt, 'ssssiis', $BusquedaTXT, $BusquedaTXT, $BusquedaTXT, $BusquedaTXT, $BusquedaINT, $BusquedaINT, $BusquedaTXT);

            if ( mysqli_stmt_execute($stmt) ) {
                $res = $stmt->get_result();
                mysqli_stmt_close($stmt);
                return $res;
            } else {
                mysqli_stmt_close($stmt);
            }

        }

        public function consultaEspNumCont($numeroDeControl) {
            $stmt = mysqli_prepare($this->conexion->getConection(), "SELECT * FROM alumnos WHERE Num_Control = ? ");
            mysqli_stmt_bind_param($stmt, 's', $numeroDeControl);

            if ( mysqli_stmt_execute($stmt) ) {
                $res = $stmt->get_result();
                mysqli_stmt_close($stmt);
                return $res;
            } else {
                mysqli_stmt_close($stmt);
            }
        }

    }
    

?>