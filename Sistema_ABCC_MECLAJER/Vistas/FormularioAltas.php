<?php
  
  session_start();

  if ( $_SESSION['valido'] == false ) {
    header("location:Login_vF/login.php");
  }

    include_once('../Scripts_php/alumno_dao.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="Requerimientos/bootstrap.min.css">
    <script src="Requerimientos/jquery.min.js"></script>
    <script src="Requerimientos/bootstrap.min.js"></script>
    <title>Nuevo Alumno</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/custom.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/noui/nouislider.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

<nav class="navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="bajas_cambios.php">ABCC BY MECLAJER-EDU</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="pantalla_inicio.php">Inicio</a></li>
      <li class="active"><a href="FormularioAltas.php">Agregar</a></li>
      <li><a href="pantalla_Eliminar.php">Eliminar</a></li>
      <li><a href="pantalla_Modificar.php">Modificar</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> -->
      <li><a href="../Scripts_php/cerrar_sesion.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>
<!-- ===================================================== CONTENIDO PANTALLA =============================================================== -->

<div class="container-contact100">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" action="../Scripts_php/procesarAlta.php" method="POST">
				<span class="contact100-form-title">
					Agregar Alumno
				</span>

				<div class="wrap-input100 validate-input bg1" data-validate="Por favor el Num. de Control">
					<span class="label-input100">Numero de control *</span>
					<input class="input100" type="text" name="cajaNumControl" placeholder="Introduzca su numero de control">
				</div>

				<div class="wrap-input100 validate-input bg1" data-validate="Por favor escriba su nombre">
					<span class="label-input100">Nombre competo *</span>
					<input class="input100" type="text" name="cajaNombre" placeholder="Introduzca su nombre">
				</div>

				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Por favor escriba su Apellido Paterno">
					<span class="label-input100">Primer Ap *</span>
					<input class="input100" type="text" name="cajaPrimAp" placeholder="Inserta tu Primer Ap. ">
				</div>

				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Por favor escriba su Apellido Materno">
					<span class="label-input100">Segundo Ap *</span>
					<input class="input100" type="text" name="cajaSegAp" placeholder="Inserta tu Segundo Ap. ">
				</div>

				<div class="wrap-input100 validate-input bg1" data-validate="Por favor escriba su edad">
					<span class="label-input100">Edad *</span>
					<input class="input100" type="number" name="cajaEdad" placeholder="Introduzca su edad">
				</div>

				<div class="wrap-input100 validate-input bg1" data-validate="Por favor escriba su semestre">
					<span class="label-input100">Semestre *</span>
					<input class="input100" type="number" name="cajaSem" placeholder="Introduzca su edad">
				</div>

				<div class="wrap-input100 input100-select bg1">
					<span class="label-input100">Carrera *</span>
					<div>
						<select class="js-select2" name="cajaCarr">
							<option>I.S.C.</option>
							<option>I.I.A.</option>
							<option>I.M.</option>
							<option>L.A.</option>
							<option>C.P.</option>
						</select>
						<div class="dropDownSelect2"></div>
					</div>
				</div>

				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn">
						<span>
							Enviar
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
			</form>
		</div>
	</div>

	
<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});


			// $(".js-select2").each(function(){
			// 	$(this).on('select2:close', function (e){
			// 		if($(this).val() == "Please chooses") {
			// 			$('.js-show-service').slideUp();
			// 		}
			// 		else {
			// 			$('.js-show-service').slideUp();
			// 			$('.js-show-service').slideDown();
			// 		}
			// 	});
			// });
		})
	</script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="vendor/noui/nouislider.min.js"></script>
	<script>
	    var filterBar = document.getElementById('filter-bar');

	    noUiSlider.create(filterBar, {
	        start: [ 1500, 3900 ],
	        connect: true,
	        range: {
	            'min': 1500,
	            'max': 7500
	        }
	    });

	    var skipValues = [
	    document.getElementById('value-lower'),
	    document.getElementById('value-upper')
	    ];

	    filterBar.noUiSlider.on('update', function( values, handle ) {
	        skipValues[handle].innerHTML = Math.round(values[handle]);
	        $('.contact100-form-range-value input[name="from-value"]').val($('#value-lower').html());
	        $('.contact100-form-range-value input[name="to-value"]').val($('#value-upper').html());
	    });
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
    
<!-- ===================================================== FIN CONTENIDO PANTALLA =============================================================== -->
</body>
</html>

