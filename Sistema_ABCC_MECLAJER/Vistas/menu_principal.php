<?php
  
  session_start();

  if ( $_SESSION['valido'] == false ) {
    header("location:Login_vF/login.php");
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sistema ABCC</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="Requerimientos/bootstrap.min.css">
  <script src="Requerimientos/jquery.min.js"></script>
  <script src="Requerimientos/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="bajas_cambios.php">ABCC BY MECLAJER-EDU</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="bajas_cambios.php">Inicio</a></li>
      <li><a href="FormularioAltas.php">Agregar</a></li>
      <li><a href="bajas_cambios.php">Eliminar</a></li>
      <li><a href="FormularioModificaciones.php">Modificar</a></li>
      <li><a href="#">Buscar</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> -->
      <li><a href="../Scripts_php/cerrar_sesion.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>
</body>
</html>
