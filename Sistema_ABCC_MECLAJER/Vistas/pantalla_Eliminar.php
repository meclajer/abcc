<?php

    include_once('../Scripts_php/alumno_dao.php');  
  
    session_start();

    if ( $_SESSION['valido'] == false ) {
        header("location:Login_vF/login.php");
    }

?>

<script type="text/javascript">
    function borrar() {
        return window.confirm( '¿Borrar?' );
    }
</script>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="Requerimientos/bootstrap.min.css">
    <script src="Requerimientos/jquery.min.js"></script>
    <script src="Requerimientos/bootstrap.min.js"></script>
    <title>Eliminar Alumnos</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/custom.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="bajas_cambios.php">ABCC BY MECLAJER-EDU</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="pantalla_inicio.php">Inicio</a></li>
      <li><a href="FormularioAltas.php">Agregar</a></li>
      <li class="active"><a href="pantalla_Eliminar.php">Eliminar</a></li>
      <li><a href="pantalla_Modificar.php">Modificar</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="../Scripts_php/cerrar_sesion.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>
<!-- ===================================================== CONTENIDO PANTALLA =============================================================== -->

<div class="container">
        <form action="../Scripts_php/procesar_ConsultaE.php" method="POST">
            <h1 class="page-header" style="text-align: center;">ALUMNOS REGISTRADOS</h1>
            <h3 class="padding-left">Encontrar con:</h3>
            <input class="form-control fa fa-search" type="text" name="buscarA" placeholder="A Buscar">
            <span><br></span>
            <input class="btn btn-primary form-control" type="submit" value="Buscar" >
                
            </i>
        </form>
    </div>

    

    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Listado de  <b>ALUMNOS</b></h2></div>
                    <!-- <div class="col-sm-4">
                        <a href="create.php" class="btn btn-info add-new">
                        	<i class="fa fa-plus"></i> 
                        	Agregar ALUMNO
                        </a>
                    </div> -->
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Numero de Control</th>
                        <th>Nombre(s)</th>
                        <th>Primer Ap.</th>
						<th>Segundo Ap.</th>
                        <th>Edad</th>
                        <th>Semestre</th>
                        <th>Carrera</th>
                        <th>REALIZAR</th>
                    </tr>
                </thead>
                 
                <tbody>    
                        <?php

                            $aDAO = new AlumnoDAO();
                            
                            if ( isset( $_SESSION['espesifica'] ) ) {
                                $listaAlumnos = $aDAO->consultaEspesifica( $_SESSION['espesifica'] );
                            } else {
                                
							    $listaAlumnos = $aDAO->mostrarTodos();
                            }

							while ($fila=mysqli_fetch_object($listaAlumnos)){
									$nc = $fila->Num_Control;
									$n = $fila->Nombre_Alumno;
									$pa = $fila->Primer_Ap_Alumno;
									$sa = $fila->Segundo_Ap_Alumno;
									$e = $fila->edad;
									$s = $fila->Semestre;
									$c = $fila->Carrera;
								?>
								<tr>
									<td><?php echo $nc;?></td>
									<td><?php echo $n;?></td>
									<td><?php echo $pa;?></td>
									<td><?php echo $sa;?></td>
									<td><?php echo $e;?></td>
									<td><?php echo $s;?></td>
									<td><?php echo $c;?></td>
									
									<td style="text-align: center;">
										<a href="" class="edit" title="Edición" data-toggle="tooltip">
											<!-- <i class="material-icons">&#xE254;</i> -->
                                        </a>
                                        <form action="<?php $_SERVER[ 'PHP_SELF' ]; header('location:../Scripts_php/procesar_bajas.php?NumControl='.$nc);?>" onclick="return borrar()" method="GET">
                                            <a href="../Scripts_php/procesar_bajas.php?NumControl=<?php echo $nc;?>" class="delete" title="Eliminación" data-toggle="tooltip">

                <i class="fa fa-trash" style="font-size:30px;color:red; padding-left: 30px;"></i>											
                                            </a>
                                        </form>
									</td>
								</tr>	
								<?php
							}
								?>  
                </tbody>
            </table>

        </div>
    </div>
<!-- ===================================================== FIN CONTENIDO PANTALLA =============================================================== -->
</body>
</html>
