<?php
  
  session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Altas</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=0.75">

	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/noui/nouislider.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" action="../Scripts_php/procesar_AltaUsuario.php" method="POST">
				<span class="contact100-form-title">
					Registrar Usuario
				</span>

				<div class="wrap-input100 bg1">
					<span class="label-input100">Numero de control *</span>
					<input class="input100" type="text" name="cajaNumControl" placeholder="Introduzca su numero de control" value="<?php if ( isset($_SESSION['usuario']) ) { echo $_SESSION['usuario']; } ?>" require >
					<span class="input100" style="color: red;"><?php if ( isset($_SESSION['error_usuario']) ) { echo $_SESSION['error_usuario'];  } ?></span>
				</div>

				<div class="wrap-input100 bg1">
					<span class="label-input100">Contraseña *</span>
					<input class="input100" type="text" name="cajaContrasena" placeholder="Introduzca su contraseña" value="<?php if ( isset($_SESSION['contrasena']) ) { echo $_SESSION['contrasena']; } ?>" require >
					<span class="input100" style="color: red;"><?php if ( isset($_SESSION['error_password']) ) { echo $_SESSION['error_password']; } ?></span>
				</div>

				<div class="wrap-input100 bg1">
					<span class="label-input100">Confirmar contraseña *</span>
					<input class="input100" type="text" name="confirmarCajaContrasena" placeholder="Introduzca su contraseña" value="<?php if ( isset($_SESSION['confirmacion']) ) { echo $_SESSION['confirmacion']; } ?>" require >
					<span class="input100" style="color: red;"><?php if ( isset($_SESSION['error_confirmacion']) ) { echo $_SESSION['error_confirmacion']; } ?></span>
				</div>

				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn">
						<span>
							Enviar
							<!-- <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i> -->
						</span>
					</button>
				</div>
			</form>
			<form action="Login_VF/login.php">
				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn">
						<span>
							Regresar
							<!-- <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i> -->
						</span>
					</button>
				</div>
			</form>
		</div>
	</div>

	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/noui/nouislider.min.js"></script>
	<script>
	    var filterBar = document.getElementById('filter-bar');

	    noUiSlider.create(filterBar, {
	        start: [ 1500, 3900 ],
	        connect: true,
	        range: {
	            'min': 1500,
	            'max': 7500
	        }
	    });

	    var skipValues = [
	    document.getElementById('value-lower'),
	    document.getElementById('value-upper')
	    ];

	    filterBar.noUiSlider.on('update', function( values, handle ) {
	        skipValues[handle].innerHTML = Math.round(values[handle]);
	        $('.contact100-form-range-value input[name="from-value"]').val($('#value-lower').html());
	        $('.contact100-form-range-value input[name="to-value"]').val($('#value-upper').html());
	    });
	</script>
	<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>

<?php

session_unset();

?>
