create database escuelaweb;

create database usuarios_escuela_web;

use escuelaweb;

CREATE TABLE alumnos ( Num_Control varchar(10) NOT NULL, Nombre_Alumno varchar(50) NOT NULL, Primer_Ap_Alumno varchar(50) NOT NULL, Segundo_Ap_Alumno varchar(50) NOT NULL, edad tinyint(4) NOT NULL, Semestre tinyint(4) NOT NULL, Carrera varchar(50) NOT NULL, PRIMARY KEY (Num_Control));

use usuarios_escuela_web;

CREATE TABLE usuarios (nombre_usuario varchar(100) NOT NULL, contrasena varchar(100) NOT NULL, PRIMARY KEY (nombre_usuario));